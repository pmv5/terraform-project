output "rds_endpoint" {
  description = "The endpoint of mysql RDS"
  value       = aws_db_instance.rds_mysql_database.address
}

output "ami_id_ubuntu" {
  description = "ami of ubuntu server"
  value       = data.aws_ami.ubuntu_ami.id
}

output "rds_mysql_apache_subnet_group_id" {
  description = "ID of DB subnet group"
  value       = aws_db_subnet_group.mysql_rds_subnet_group.id
}

output "public_subnet_ids" {
  description = "IDs of public subnets"
  value       = data.aws_subnets.public_subnet_ids.ids
}

output "private_subnet_ids" {
  description = "IDs of private subnets"
  value       = data.aws_subnets.private_subnet_ids.ids
}