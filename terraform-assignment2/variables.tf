variable "aws_region" {
  description = "Default aws region"
  type        = string
  default     = "us-east-1"
}
variable "username" {
  description = "A variable which stores the username of the RDS database"
}

variable "password" {
  description = "A variable which stores the password of the RDS database"
}

variable "subnets" {
  type = map(any)
  default = {
    sub_1 = {
      az        = "us-east-1a"
      cidr      = "10.0.0.0/24"
      public_ip = "true"
      name      = "apache-subnet-public-1"
      type      = "public"
    }
    sub_2 = {
      az        = "us-east-1b"
      cidr      = "10.0.1.0/24"
      public_ip = "true"
      name      = "apache-subnet-public-2"
      type      = "public"
    }
    sub_3 = {
      az        = "us-east-1c"
      cidr      = "10.0.2.0/24"
      public_ip = "false"
      name      = "rds-subnet-private-1"
      type      = "private"
    }
    sub_4 = {
      az        = "us-east-1d"
      cidr      = "10.0.3.0/24"
      public_ip = "false"
      name      = "rds-subnet-private-2"
      type      = "private"
    }
  }
}

variable "rds_sg_tag" {
  description = "Name of the rds security group"
  default     = "MV_RDS_sg"
}

variable "rds_mysql_port" {
  description = "Mysql port"
  default     = 3306
}

variable "anywhere_cidr" {
  description = "Anywhere cidr block"
  default     = "0.0.0.0/0"
}