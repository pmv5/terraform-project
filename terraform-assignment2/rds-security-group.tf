resource "aws_security_group" "rds_sg" {
  name   = var.rds_sg_tag
  vpc_id = data.aws_vpc.vpc_id.id

  ingress {
    security_groups = [module.ec2_module.apache_sg]
    from_port       = var.rds_mysql_port
    to_port         = var.rds_mysql_port
    protocol        = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = [var.anywhere_cidr]
  }

  tags = {
    Name = var.rds_sg_tag
  }
}
