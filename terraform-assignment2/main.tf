module "vpc_module" {
  source      = "./modules/vpc"
  cidr_block  = "10.0.0.0/16"
  vpc_name    = "apache_vpc"
  environment = "training"
}

module "subnet_module" {
  source             = "./modules/subnets"
  vpc_id             = data.aws_vpc.vpc_id.id
  for_each           = var.subnets
  cidr_subnets       = each.value["cidr"]
  map_public_ip      = each.value["public_ip"]
  availability_zones = each.value["az"]
  subnet_name        = each.value["name"]
  subnet_type        = each.value["type"]
  environment        = "training"
}

module "ec2_module" {
  source               = "./modules/ec2"
  count_of_instances   = 2
  user                 = "ubuntu"
  apache_ami           = data.aws_ami.ubuntu_ami.id
  public_subnet_id     = data.aws_subnets.public_subnet_ids.ids
  apache_vpc           = data.aws_vpc.vpc_id.id
  rds_database_address = aws_db_instance.rds_mysql_database.address
  myip                 = "157.46.100.251/32"
  anywhere_cidr        = var.anywhere_cidr
  ec2_tag_name         = "MV-apache-terraform"
  lb_tag_name          = "apache_lb"
  apache_sg_tag        = "apache_sg"
  lb_sg_tag            = "lb_sg"
}

module "network_module" {
  source                      = "./modules/network"
  vpc_id                      = data.aws_vpc.vpc_id.id
  rt_association_subnet_count = 2
  public_subnet_id            = data.aws_subnets.public_subnet_ids.ids
  igw_tag                     = "apache_igw"
  rt_tag                      = "apache_public_rt"

}

resource "aws_db_subnet_group" "mysql_rds_subnet_group" {
  name       = "mysql_rds_subnet_group"
  subnet_ids = data.aws_subnets.private_subnet_ids.ids

  tags = {
    Name = "mysql_rds_subnet_group"
  }
}

resource "aws_db_instance" "rds_mysql_database" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  username               = var.username
  password               = var.password
  skip_final_snapshot    = true
  port                   = 3306
  availability_zone      = "us-east-1c"
  db_name                = "apachedb"
  db_subnet_group_name   = aws_db_subnet_group.mysql_rds_subnet_group.name
  identifier             = "rds-mysql-database"
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  tags = {
    Name = "rds-mysql-database"
  }
}