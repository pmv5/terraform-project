data "aws_ami" "ubuntu_ami" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

data "aws_vpc" "vpc_id" {
  id = module.vpc_module.apache_vpc
}

data "aws_subnets" "public_subnet_ids" {
  filter {
    name   = "vpc-id"
    values = [module.vpc_module.apache_vpc]
  }

  tags = {
    type        = "public"
    environment = "training"
  }

}

data "aws_subnets" "private_subnet_ids" {
  filter {
    name   = "vpc-id"
    values = [module.vpc_module.apache_vpc]
  }

  tags = {
    type        = "private"
    environment = "training"
  }
}

