## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | ~> 4.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.29.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_ec2_module"></a> [ec2\_module](#module\_ec2\_module) | ./modules/ec2 | n/a |
| <a name="module_network_module"></a> [network\_module](#module\_network\_module) | ./modules/network | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_db_instance.rds_mysql_database](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance) | resource |
| [aws_db_subnet_group.rds_mysql_subnet_group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_subnet_group) | resource |
| [aws_security_group.rds_sg](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_ami.ubuntu_ami](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_subnets.private_subnet_ids](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_subnets.public_subnet_ids](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/subnets) | data source |
| [aws_vpc.vpc_id](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/vpc) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_region"></a> [aws\_region](#input\_aws\_region) | Default aws region | `string` | `"us-east-1"` | no |
| <a name="input_cidr_private_subnets"></a> [cidr\_private\_subnets](#input\_cidr\_private\_subnets) | A list of cidrs to create two private subnets | `list(any)` | <pre>[<br>  "10.0.2.0/24",<br>  "10.0.3.0/24"<br>]</pre> | no |
| <a name="input_cidr_public_subnets"></a> [cidr\_public\_subnets](#input\_cidr\_public\_subnets) | A list of cidrs to create two public subnets | `list(any)` | <pre>[<br>  "10.0.0.0/24",<br>  "10.0.1.0/24"<br>]</pre> | no |
| <a name="input_password"></a> [password](#input\_password) | A variable which stores the password of the RDS database | `string` | `""` | no |
| <a name="input_username"></a> [username](#input\_username) | A variable which stores the username of the RDS database | `string` | `""` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ami_id_ubuntu"></a> [ami\_id\_ubuntu](#output\_ami\_id\_ubuntu) | ami of ubuntu server |
| <a name="output_rds_endpoint"></a> [rds\_endpoint](#output\_rds\_endpoint) | The endpoint of mysql RDS |
| <a name="output_rds_mysql_apache_subnet_group_id"></a> [rds\_mysql\_apache\_subnet\_group\_id](#output\_rds\_mysql\_apache\_subnet\_group\_id) | ID of DB subnet group |
