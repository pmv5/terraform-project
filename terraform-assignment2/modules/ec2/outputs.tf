output "lb_dns_name" {
  description = "The dns name of application load balancer"
  value       = aws_alb.apache_alb.dns_name
}

output "apache_sg" {
  description = "The ID of apache web server security group"
  value       = aws_security_group.apache_sg.id
}