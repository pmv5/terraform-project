variable "apache_instance_type" {
  description = "Default instance type of ubuntu server"
  type        = string
  default     = "t2.micro"
}

variable "apache_ami" {
  description = "The ami of ubuntu server"
}

variable "public_subnet_id" {
  description = "The ID of public subnet"
}

variable "keypair_name" {
  description = "Keypair name"
  default     = "MV-apache-keypair"
}

variable "apache_vpc" {
  description = "ID of vpc"
}

variable "rds_database_address" {
  description = "Endpoint of rds database"
}

variable "count_of_instances" {
  description = "Number of instances"
}

variable "user" {
  description = "User of the instance"
}

variable "ec2_tag_name" {
  description = "Name of the apache web server"
}

variable "myip" {
  description = "My IP"
}

variable "anywhere_cidr" {
  description = "Anywhere cidr block"
}

variable "lb_tag_name" {
  description = "Name of the application load balancer"
}

variable "apache_sg_tag" {
  description = "Name of the apache web server security group "
}

variable "lb_sg_tag" {
  description = "Name of the application load balancer security group"
}
