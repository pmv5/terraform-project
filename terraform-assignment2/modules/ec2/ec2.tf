resource "aws_instance" "apache_web_server" {
  count                  = var.count_of_instances
  ami                    = var.apache_ami
  instance_type          = var.apache_instance_type
  subnet_id              = var.public_subnet_id[0]
  key_name               = var.keypair_name
  vpc_security_group_ids = [aws_security_group.apache_sg.id]
  connection {
    type        = "ssh"
    user        = var.user
    private_key = file("./MV-apache-keypair.pem")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "./index.php"
    destination = "/home/ubuntu/index.php"
  }

  provisioner "file" {
    source      = "./username.txt"
    destination = "/home/ubuntu/username.txt"
  }

  provisioner "file" {
    source      = "./password.txt"
    destination = "/home/ubuntu/password.txt"
  }

  provisioner "file" {
    content     = templatefile("rds.txt.tpl", { db_host = var.rds_database_address })
    destination = "/home/ubuntu/rds-endpoint.txt"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt -y update",
      "sudo apt -y install apache2",
      "sudo apt -y install php-mysql",
      "sudo apt -y install php",
      "sudo apt -y install mysql-server",
      "sudo systemctl start apache2",
      "sudo mv /home/ubuntu/index.php /var/www/html/index.php",
      "sudo rm /var/www/html/index.html"
    ]
  }

  tags = {
    Name = "${var.ec2_tag_name}-${count.index}"
  }
}

resource "aws_alb" "apache_alb" {
  name            = "apache-alb"
  security_groups = [aws_security_group.lb_sg.id]
  subnets         = var.public_subnet_id
  depends_on = [
    aws_instance.apache_web_server
  ]
  tags = {
    Name = var.lb_tag_name
  }
}

resource "aws_alb_target_group" "apache_alb_target_group" {
  name        = "apache-alb-target-group"
  port        = 80
  target_type = "instance"
  protocol    = "HTTP"
  vpc_id      = var.apache_vpc

  health_check {
    path = "/"
    port = 80
  }
}

resource "aws_alb_listener" "apache_alb_listener" {
  load_balancer_arn = aws_alb.apache_alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_alb_target_group.apache_alb_target_group.arn
    type             = "forward"
  }
}

resource "aws_lb_target_group_attachment" "apache_alb_attachment" {
  count            = 2
  target_group_arn = aws_alb_target_group.apache_alb_target_group.arn
  target_id        = aws_instance.apache_web_server[count.index].id
  port             = 80
}