output "public_subnet_id" {
  description = "IDs of public and private subnets"
  value       = aws_subnet.apache_rds_subnets.id
}
