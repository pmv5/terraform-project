variable "vpc_id" {
  description = "ID of vpc"
}

variable "cidr_subnets" {
  description = "cidr of private and public subnets"
}

variable "availability_zones" {
  description = "availability zones of private and public subnets"
}

variable "environment" {
  description = "Environment"
}

variable "subnet_type" {
  description = "Type of subnet either public or private"
}

variable "subnet_name" {
  description = "Name of private and public subnets"
}

variable "map_public_ip" {
  description = "Public IP mapping to subnet"
}