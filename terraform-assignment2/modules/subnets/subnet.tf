resource "aws_subnet" "apache_rds_subnets" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.cidr_subnets
  map_public_ip_on_launch = var.map_public_ip
  availability_zone       = var.availability_zones

  tags = {
    Name        = var.subnet_name
    type        = var.subnet_type
    environment = var.environment
  }
}