variable "cidr_block" {
  description = "The cidr block of vpc"
}

variable "vpc_name" {
  description = "Name of vpc"
}

variable "environment" {
  description = "Environment"
}

