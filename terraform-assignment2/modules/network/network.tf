resource "aws_internet_gateway" "apache_igw" {
  vpc_id = var.vpc_id

  tags = {
    Name = var.igw_tag
  }
}

resource "aws_route_table" "apache_public_rt" {
  vpc_id = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.apache_igw.id
  }
  tags = {
    Name = var.rt_tag
  }
}

resource "aws_route_table_association" "apache_rta_public_subnet" {
  count          = var.rt_association_subnet_count
  subnet_id      = var.public_subnet_id[count.index]
  route_table_id = aws_route_table.apache_public_rt.id
}