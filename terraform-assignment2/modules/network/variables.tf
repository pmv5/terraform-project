variable "vpc_id" {
  description = "ID of vpc"
}

variable "public_subnet_id" {
  description = "The ID of public subnet"
}

variable "rt_association_subnet_count" {
  description = "Number of subnets to be associated to the route table"
}

variable "igw_tag" {
  description = "Name of the internet gateway"
}

variable "rt_tag" {
  description = "Name of the public route table"
}