variable "aws_region" {
  type        = string
  description = "Default aws region"
  default     = "us-west-2"
}

variable "instance_type" {
  type        = string
  description = "Default instance type of ubuntu server"
  default     = "t2.micro"
}

variable "cidr_public_subnets" {
  type        = list(any)
  description = "A list of cidrs to create two public subnets"
  default     = ["10.0.0.0/24", "10.0.1.0/24"]
}

variable "cidr_private_subnets" {
  type        = list(any)
  description = "A list of cidrs to create two private subnets"
  default     = ["10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_availability_zones" {
  type        = list(any)
  description = "A list of availability zones to create two public subnets"
  default     = ["us-west-2a", "us-west-2b"]
}

variable "private_availability_zones" {
  type        = list(any)
  description = "A list of availability zones to create two private subnets"
  default     = ["us-west-2c", "us-west-2d"]
}

variable "username" {
  description = "A variable which stores the username of the RDS database"
  type        = string
  default     = ""
}

variable "password" {
  description = "A variable which stores the password of the RDS database"
  type        = string
  default     = ""
}

variable "keypair" {
  description = "The name of the keypair.pem file"
  type        = string
  default     = "MV-VPN-Keypair"
}

variable "inbound_source" {
  description = "Allowed CIDR"
  type        = string
  default     = "49.249.37.158/32"
}