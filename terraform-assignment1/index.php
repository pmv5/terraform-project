<?php 
    // Obtain host, username and password from file
    $host = file_get_contents("/home/ubuntu/rds-endpoint.txt");
    $user     = file_get_contents("/home/ubuntu/username.txt");
    $password = file_get_contents("/home/ubuntu/password.txt");
    $db       = 'apachedb';
    $hostname = gethostname();

    // Create connection to mysql database
    $conn     = new mysqli($host, $user, $password,$db);

    // Testing connection
    echo "Testing connection...";
    echo "</br>";  
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    echo "Connected to MySQL successfully!";
    echo "</br>";  
    echo $hostname;
    $conn->close();
?>