resource "aws_internet_gateway" "apache_igw" {
  vpc_id = aws_vpc.apache_vpc.id

  tags = {
    Name = "apache-igw"
  }
}

resource "aws_route_table" "apache_public_rt" {
  vpc_id = aws_vpc.apache_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.apache_igw.id
  }

  tags = {
    Name = "apache-public-rt"
  }
}

resource "aws_route_table_association" "apache_rta_public_subnet" {
  count          = 2
  subnet_id      = element(aws_subnet.apache_public_subnet.*.id, count.index)
  route_table_id = aws_route_table.apache_public_rt.id
}

resource "aws_security_group" "apache_sg" {
  vpc_id = aws_vpc.apache_vpc.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.inbound_source]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [var.inbound_source]
  }

  tags = {
    Name = "apache-sg"
  }
}

resource "aws_security_group" "rds_sg" {
  name   = "MV-rds-sg"
  vpc_id = aws_vpc.apache_vpc.id

  ingress {
    description     = "MYSQL"
    security_groups = [aws_security_group.apache_sg.id]
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "MV-RDS-sg"
  }
}
