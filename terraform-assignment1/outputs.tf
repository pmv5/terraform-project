output "lb_dns_name" {
  description = "The dns name of application load balancer"
  value       = aws_elb.apache_lb.dns_name
}

output "rds_endpoint" {
  description = "The endpoint of mysql RDS"
  value       = aws_db_instance.rds_mysql_database.address
}

output "vpc_id" {
  description = "ID of vpc"
  value       = aws_vpc.apache_vpc.id
}

output "public_subnet_id" {
  description = "ID of public subnet"
  value       = aws_subnet.apache_public_subnet.*.id
}

output "private_subnet_id" {
  description = "ID of private subnet"
  value       = aws_subnet.rds_private_subnet.*.id
}

output "ami_id_ubuntu" {
  description = "ami of ubuntu server"
  value       = data.aws_ami.ubuntu_ami
}

output "rds_mysql_apache_subnet_group_id" {
  description = "ID of DB subnet group"
  value       = aws_db_subnet_group.rds_mysql_apache_subnet_group.id
}
