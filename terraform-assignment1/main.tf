resource "aws_db_subnet_group" "rds_mysql_apache_subnet_group" {
  name       = "rds_mysql_apache_subnet_group"
  subnet_ids = aws_subnet.rds_private_subnet.*.id

  tags = {
    Name = "rds_mysql_apache_subnet_group"
  }
}

resource "aws_db_instance" "rds_mysql_database" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  username               = var.username
  password               = var.password
  skip_final_snapshot    = true
  port                   = 3306
  availability_zone      = var.private_availability_zones[0]
  db_name                = "apachedb"
  db_subnet_group_name   = aws_db_subnet_group.rds_mysql_apache_subnet_group.name
  identifier             = "mv-rds-mysql"
  vpc_security_group_ids = [aws_security_group.rds_sg.id]

  tags = {
    Name = "rds-mysql-database"
  }
}

resource "aws_instance" "apache_web_server" {
  count                  = 2
  ami                    = data.aws_ami.ubuntu_ami.id
  instance_type          = var.instance_type
  subnet_id              = element(aws_subnet.apache_public_subnet.*.id, count.index)
  vpc_security_group_ids = [aws_security_group.apache_sg.id]
  key_name               = var.keypair
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("./MV-VPN-Keypair.pem")
    host        = self.public_ip
  }
  provisioner "file" {
    source      = "./index.php"
    destination = "/home/ubuntu/index.php"
  }

  provisioner "file" {
    source      = "./username.txt"
    destination = "/home/ubuntu/username.txt"
  }

  provisioner "file" {
    source      = "./password.txt"
    destination = "/home/ubuntu/password.txt"
  }

  provisioner "file" {
    content     = templatefile("rds.txt.tpl", { db_host = aws_db_instance.rds_mysql_database.address })
    destination = "/home/ubuntu/rds-endpoint.txt"
  }

  user_data = file("apache-installation.sh")

  tags = {
    Name = "MV-apache-terraform-${count.index}"
  }
}

resource "aws_elb" "apache_lb" {
  name                        = "apache-lb"
  subnets                     = aws_subnet.apache_public_subnet.*.id
  security_groups             = [aws_security_group.apache_sg.id]
  instances                   = aws_instance.apache_web_server.*.id
  idle_timeout                = 100
  connection_draining         = true
  connection_draining_timeout = 300

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/index.php"
    interval            = 30
  }

  tags = {
    Name = "apache_lb"
  }
}