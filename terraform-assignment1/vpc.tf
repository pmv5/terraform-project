resource "aws_vpc" "apache_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_support   = "true"
  enable_dns_hostnames = "true"
  instance_tenancy     = "default"

  tags = {
    Name = "apache-vpc"
  }
}

resource "aws_subnet" "apache_public_subnet" {
  count                   = 2
  vpc_id                  = aws_vpc.apache_vpc.id
  cidr_block              = var.cidr_public_subnets[count.index]
  map_public_ip_on_launch = "true"
  availability_zone       = var.public_availability_zones[count.index]

  tags = {
    Name = "apache-subnet-public-${count.index}"
  }
}

resource "aws_subnet" "rds_private_subnet" {
  count             = 2
  vpc_id            = aws_vpc.apache_vpc.id
  cidr_block        = var.cidr_private_subnets[count.index]
  availability_zone = var.private_availability_zones[count.index]

  tags = {
    Name = "rds-subnet-private-${count.index}"
  }
}